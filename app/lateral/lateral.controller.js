(function(){
    "use strict";
    
    angular
        .module("banamexApp")
        .controller("lateralCtrl", lateralCtrl);

    lateralCtrl.$inject = ["$rootScope", "$scope", "$mdSidenav", "$location", "descargasSrvc"];
    function lateralCtrl($rootScope, $scope, $mdSidenav, $location, descargasSrvc) {
        var lc = this;

        lc.listado = [];
        lc.activeItem = null;
        lc.openMenu = openMenu;
        lc.closeMenu = closeMenu;
        lc.detalle = detalle;
        lc.goHome = goHome;
        
        function openMenu() {
            $mdSidenav("left").toggle();
        }
        
        function closeMenu() {
            $mdSidenav('left').close();
        }
        
        function goHome() {
            $location.path('/home');
        }
        
        function detalle(_item) {
            $rootScope.item = _item;
            lc.activeItem = _item;
            closeMenu();
            $location.path('/home/'+_item.$id);
        }

        descargasSrvc.getDescargas().then(function(){
            lc.listado = descargasSrvc.listado;
        });
        
        if($rootScope.item != undefined) {
           lc.activeItem = $rootScope.item; 
        }
    }

})();