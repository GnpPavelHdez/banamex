(function(){
    "use strict";
    
    angular
        .module("banamexApp")
        .factory("Auth",authFactory);
    
    authFactory.$inject = ["$firebaseAuth", "$location"];
    function authFactory ($firebaseAuth, $location) {
        var fauth = $firebaseAuth();
        
        fauth.$onAuthStateChanged(function(user) {
            if (!user) {
                $location.path("home");
      		}
        });
        return fauth;
    }
})();