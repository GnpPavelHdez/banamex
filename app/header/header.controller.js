(function(){
    "use strict";
    
    angular
        .module("banamexApp")
        .controller("headerCtrl", headerCtrl);

    headerCtrl.$inject = ["$rootScope", "$scope", "$mdSidenav"];
    function headerCtrl($rootScope, $scope, $mdSidenav) {
        var hc = this;
        
        hc.titulo = null;
        hc.openMenu = openMenu;
        
        function openMenu() {
            $mdSidenav("left").toggle();
        }
        
        if($rootScope.item != undefined) {
           hc.titulo = $rootScope.item.titulo;
        }
        
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            hc.titulo = $rootScope.item.titulo;
        });
    }

})();