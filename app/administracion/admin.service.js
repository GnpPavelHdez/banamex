(function () {
    "use strict";
    
    angular
        .module("banamexApp")
        .service("adminSrvc", adminSrvc);
        
    adminSrvc.$inject = ['$rootScope','$firebaseObject', '$firebaseArray', '$q'];
    function adminSrvc($rootScope, $firebaseObject, $firebaseArray, $q) {
        var vm = this;
        vm.getDescargas = getDescargas;
        vm.eliminar = eliminar;
        vm.guardar = guardar;
        vm.guardarItem = guardarItem;
        vm.guardarArchivo = guardarArchivo;

        function getDescargas(){
            vm.listado = [];
            vm.descargas = $firebaseArray(firebase.database().ref('descargas'));
            
            return vm.descargas.$loaded(function(data) {
                vm.listado = data;
            });
        }
        
        function eliminar(_id){
            return $q(function(resolve, reject) {
               firebase.database().ref("/descargas").child(_id).remove(function (error) {
                    if (!error) {
                        resolve(true);
                    }
                })
            });    
        }
        
        function guardar(_item, _archivo) {
            return $q(function(resolve, reject) {
                if(_archivo != null) {
                    guardarArchivo(_archivo).then(function(urlArchivo){
                        _item.archivo = urlArchivo;
                        guardarItem(_item).then(function(){
                            resolve(true);
                        });
                    });
                } else {
                    guardarItem(_item).then(function(){
                        resolve(true);
                    });
                }
            });
        }
        
        function guardarItem(_item) {
            return $q(function(resolve, reject) {
                var data = {
                    "titulo": _item.titulo,
                    "subtitulo": _item.subtitulo,
                    "contenido": _item.contenido,
                    "imagen": _item.imagen,
                    "archivo": _item.archivo
                };
                if(_item.$id) {
                    firebase.database().ref("/descargas").child(_item.$id).set(data).then(function() {
                        resolve(true);
                    });
                } else {
                    firebase.database().ref("/descargas").push(data).then(function() {
                        resolve(true);
                    });
                }
            });
        }
        
        function guardarArchivo(_archivo) {
            return $q(function(resolve, reject) {
                var uploadTask = firebase.storage().ref("/descargas").child(_archivo.name).put(_archivo, {'contentType': _archivo.type});
                uploadTask.on('state_changed',null,
                    function error(error){
                        alert(error);
                        reject();
                    },
                    function complete(){
                        resolve(uploadTask.snapshot.downloadURL);
                });
            });
        }

        $rootScope.$on("logout", function(event, args) {
            vm.listado = [];
            vm.descargas.$destroy();
        });
    }
})();
