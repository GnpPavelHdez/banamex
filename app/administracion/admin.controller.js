(function(){
    "use strict";
    
    angular
        .module("banamexApp")
        .controller("administracionCtrl", administracionCtrl);

    administracionCtrl.$inject = ["$rootScope", "$scope", "$firebaseArray", "adminSrvc"];
    function administracionCtrl($rootScope, $scope, $firebaseArray, adminSrvc) {
        
        var ac = this;
        ac.file = null;
        ac.edicion = false;
        ac.nuevo = false;
        ac.copia = null
        ac.listado = [];
        ac.detalle = detalle;
        ac.editar = editar;
        ac.cancelar = cancelar;
        ac.guardar = guardar;
        ac.borrar = borrar;
        ac.agregar = agregar;
        ac.validar = validar;
        ac.selectFile = selectFile;
        ac.cerrarSesion = cerrarSesion;
        ac.archivoTempName = null;
        ac.archivoTempFile = null;
        
        function editar() {
            ac.edicion = true;
        }
    
        function cancelar() {
            ac.file = null;
            ac.copia = null;
            ac.nuevo = false;
            ac.edicion = false;
            ac.archivoTempName = null;
            ac.archivoTempFile = null;
        }
    
        function detalle(_item) {
            cancelar();
            ac.archivoTempName = null;
            ac.archivoTempFile = null;
            ac.file = _item;
            ac.copia = angular.copy(_item);
        }
        
        function agregar() {
            cancelar();
            ac.nuevo = true;    
        }
        
        function selectFile(_id) {
            $scope.$apply(function() {
                if(angular.element(_id)[0].files.length > 0) {
                    var file = angular.element(_id)[0].files[0];
                    if(file.hasOwnProperty("name") == false) {
                        ac.archivoTempName = file.name;
                        ac.archivoTempFile = file;
                    }
                }
            });
        }

        function guardar() {
            adminSrvc.guardar(ac.copia, ac.archivoTempFile).then(function(){
                alert("Guardado!!!");
                ac.cancelar();
            });
        }
        
        function validar() {
            if(ac.archivoTempFile && ac.archivoTempName) {
                guardar();
            } else {
                alert("Seleccione un archivo para descargar");
            }
            
        }
        
        function borrar() {
            adminSrvc.eliminar(ac.file.$id).then(function(){
                ac.cancelar();
            });
        }
        
        function cerrarSesion() {
            firebase.auth().signOut();
        }
        
        adminSrvc.getDescargas().then(function(){
            ac.listado = adminSrvc.listado;
        });

        ac.tinymceOptions = {
            plugins: 'link image code',
            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
        };

    }

})();