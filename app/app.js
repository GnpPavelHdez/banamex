(function() {
    "use strict";

    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyASOQKmV2GCciXtT4pPNqAM34BRIBXp6k0",
        authDomain: "comentarios-60cf7.firebaseapp.com",
        databaseURL: "https://comentarios-60cf7.firebaseio.com",
        projectId: "comentarios-60cf7",
        storageBucket: "comentarios-60cf7.appspot.com",
        messagingSenderId: "546360697638"
    };
    
    firebase.initializeApp(config);

    angular
        .module("banamexApp", ["firebase", "ngMaterial", "ngMdIcons", "ngMessages", "ngIdle", "ui.router", "ngSanitize", "ui.tinymce"])
        .config(fnConfig);

    fnConfig.$inject = ["$mdGestureProvider", "$mdThemingProvider"];
    function fnConfig($mdGestureProvider, $mdThemingProvider) {
        $mdGestureProvider.skipClickHijack();

        $mdThemingProvider.definePalette('customPrimary', {
          '50': 'ffeee4',
          '100': 'ffd4bc',
          '200': 'ffb790',
          '300': 'ff9a63',
          '400': 'ff8541',
          '500': 'ff6f20',
          '600': 'ff671c',
          '700': 'ff5c18',
          '800': 'ff5213',
          '900': 'ff400b',
          'A100': 'ffffff',
          'A200': 'fff7f6',
          'A400': 'ffcdc3',
          'A700': 'ffb8a9',
          'contrastDefaultColor': 'light',
          'contrastDarkColors': [
            '50',
            '100',
            '200',
            '300',
            '400',
            'A100',
            'A200',
            'A400',
            'A700'
          ],
          'contrastLightColors': [
            '500',
            '600',
            '700',
            '800',
            '900'
          ]
        });

        $mdThemingProvider.theme('tema-gnp', 'default')
            .primaryPalette('customPrimary');
    }

})();