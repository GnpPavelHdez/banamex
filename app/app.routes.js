(function(){
    "use strict";

    angular
        .module("banamexApp")
        .config(ruteo);

        ruteo.$inject = ['$stateProvider', '$urlRouterProvider', 'AuthProvider'];
        function ruteo($stateProvider, $urlRouterProvider, Auth) {
            $urlRouterProvider.when('', '/home');
            $urlRouterProvider.when('/', '/home');
            $urlRouterProvider.otherwise('/home');

            $stateProvider
                .state('public', {
                    url: '',
                    abstract: true,
                    views: {
                        '' : {
                            templateUrl: 'app/template/master.tpl.html'
                        },
                        'lateral@public' : {
                            templateUrl: 'app/lateral/lateral.tpl.html',
                            controller: 'lateralCtrl',
                            controllerAs: 'lc'
                        },
                        'header@public' : {
                            templateUrl: 'app/header/header.tpl.html',
                            controller: 'headerCtrl',
                            controllerAs: 'hc'
                        },
                        'container@public': {
                            templateUrl: 'app/template/landing.tpl.html',
                        }
                    }
                })
                .state('public.home', {
                    parent: 'public',
                    url: '/home'
                })
                .state('public.home.detalle', {
                    parent: 'public',
                    url: '/home/:id',
                    views: {
                        'container': {
                            templateUrl: 'app/contenido/contenido.tpl.html',
                            controller: 'contenidoCtrl',
                            controllerAs: 'cc'
                        }
                    },
                    resolve: {
                        itemDescarga: ['$stateParams', '$rootScope', 'descargasSrvc', function($stateParams, $rootScope, descargasSrvc) {
                            if($rootScope.item == undefined) {
                                return descargasSrvc.getDescargasItem($stateParams.id).then(function(snapshot) {
                                    $rootScope.item = snapshot.val();
                                    $rootScope.item.$id = $stateParams.id;
                                }, function(error) {
                                    $rootScope.item = {titulo: "No se encontro la descarga"};
                                });
                            } else {
                                return $rootScope.item;
                            }
                        }]
                    }
                })
                .state('administracion', {
                    url: '/administracion',
                    views: {
                        '' : {
                            templateUrl: 'app/administracion/admin.tpl.html',
                            controller: 'administracionCtrl',
                            controllerAs: 'ac'
                        }
                    },
                    resolve: {
                        autenticacion: autenticacion
                    }
                });
            
            function autenticacion(){
                return Auth.$get().$requireSignIn();
            }
        }
})();