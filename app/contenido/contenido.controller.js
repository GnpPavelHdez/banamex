(function(){
    "use strict";
    
    angular
        .module("banamexApp")
        .controller("contenidoCtrl", contenidoCtrl);

    contenidoCtrl.$inject = ["$rootScope", "$scope", "$sce", "descargasSrvc"];
    function contenidoCtrl($rootScope, $scope, $sce, descargasSrvc) {
        
        var cc = this;
        cc.item = $rootScope.item;
        cc.trustAsHtml = trustAsHtml;
        
        function trustAsHtml(string) {
            return $sce.trustAsHtml(string);
        }   
    }

})();