(function () {
    "use strict";
    
    angular
        .module("banamexApp")
        .service("descargasSrvc", descargasSrvc);
        
    descargasSrvc.$inject = ['$rootScope','$firebaseObject', '$firebaseArray', '$q'];
    function descargasSrvc($rootScope, $firebaseObject, $firebaseArray, $q) {

        var vm = this;
        vm.getDescargas = getDescargas;
        vm.getDescargasItem = getDescargasItem;
        vm.refDescargas = firebase.database().ref('descargas')

        function getDescargas() {
            vm.listado = [];
            return $firebaseArray(vm.refDescargas).$loaded(function(data) {
                vm.listado = data;
            });
        }
        
        function getDescargasItem(_id) {
            return vm.refDescargas.child(_id).once('value');
        }

        $rootScope.$on("logout", function(event, args) {
            vm.listado = [];
            vm.descargas.$destroy();
        });
    }
})();
