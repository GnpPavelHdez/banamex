(function(){
    "use strict";
    
    angular
        .module("banamexApp")
        .controller("descargasCtrl", descargasCtrl);

    descargasCtrl.$inject = ["$rootScope", "$scope", "$sce", "$mdSidenav", "$location", "$stateParams", "$firebaseArray", "descargasSrvc"];
    function descargasCtrl($rootScope, $scope, $sce, $mdSidenav, $location, $stateParams, $firebaseArray, descargasSrvc) {
        
        var dc = this;
        dc.file = null;
        dc.listado = [];
        dc.activeItem = null;
        dc.openMenu = openMenu;
        dc.closeMenu = closeMenu;
        dc.detalle = detalle;
        dc.descarga = descarga;
        dc.goHome = goHome;
        dc.trustAsHtml = trustAsHtml;
        
        function trustAsHtml(string) {
            return $sce.trustAsHtml(string);
        };
        
        function goHome() {
            $location.path("/home");
        }

        function openMenu() {
            $mdSidenav("left").toggle();
        }
        
        function closeMenu() {
            $mdSidenav('left').close();
        }
        
        function detalle(_item) {
            dc.file = _item;
            dc.activeItem = _item;
            closeMenu();
        }
        
        function descarga(_item) {
            console.log(_item);
        }

        descargasSrvc.getDescargas().then(function(){
            dc.listado = descargasSrvc.listado;
        });

    }

})();